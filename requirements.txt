certifi==2021.5.30
chardet==3.0.4
idna==2.9
lxml==4.6.3
requests==2.23.0
urllib3==1.25.9
